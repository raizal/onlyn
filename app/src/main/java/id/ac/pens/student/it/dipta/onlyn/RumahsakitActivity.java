package id.ac.pens.student.it.dipta.onlyn;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class RumahsakitActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rumahsakit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        ListView list;
        final String[] itemname ={
                "RSAL Tanjung Perak",
                "RS AAL",
                "RS AL Pal",
                "RSB Adi Guna",
                "RS Al-Irsyad",
                "RS Pelabuhan",
                "RS Muhammadiyah",
                "RSUD Muh. Soewandi",
                "RSU Haji",
                "RSB St Melania",
                "RSB Pura Raharja",
                "RS Siloam Gleneagles",
                "RS Gotong Royong",
                "RSJ Menur",
                "RSA & B Siti Aisyah",
                "RS Putri",
                "RS Surabaya Internasional",
                "RSAD",
                "RSAL. Dr. Ramelan",
                "RSB Kartika Jaya",
                "RS Bhakti Rahayu",
                "RS Bhayangkara",
                "RS Griya Husada",
                "RS Islam Surabaya",
                "RS Islam Surabaya",
                "RS Sumber Kasih",
                "RSUD Dr. Soetomo",
                "RS Husada Utama",
                "RS Lanu Surabaya",
                "RS Adi Husada Kapasari",
                "RS Adi Husada Undaan Wetan",
                "RS Darmo",
                "RS Katolik St Vincentius A Paulo",
                "RS Mardi Sentosa",
                "RS Penyakit Dalam Mukti Mulia",
                "RS William Booth",
                "RS Mata Undaan",
                "RS Marinir Gunung Sari",
                "RS Islam",
                "RS Mitra Keluarga",
        };

        Integer[] imgid={
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
                R.drawable.rs,
        };

        final String[] Description ={
                "Jl. Laksdya M Nasir No.56, Surabaya - Telp.(031)–3293536 Fax.031–376506",
                "Jl. Moro Krembangan, Surabaya - Telp.(031)–3291092",
                "Jl. Taruna No.66–68 Ujung, Surabaya",
                "Jl. Alun-alun Rangkah No.1–3, Surabaya - Telp.(031)–3711851",
                "Jl. KH M Mansyur No.210–214, Surabaya - Telp.(031)–3531223",
                "Jl. Kalianget No.1–2, Surabaya - Telp.(031)–3294801",
                "Jl. KH M Mansyur No.180–182, Surabaya - Telp.(031)–3522980",
                "Jl. Tambak Rejo No.45-47, Surabaya - Telp.(031)–3717141",
                "Jl. Manyar Kertoadi, Surabaya - Telp.(031)–5947760",
                "Jl. Tambaksari No.7, Surabaya - Telp.(031)–5032854",
                "Jl. Pucang Adi No.12–14, Surabaya - Telp.(031)–5022151",
                "Jl. Raya Gubeng No. 70, Surabaya - Telp.(031)–5031333",
                "Jl. Manyar Kartika IV No.2–6, Surabaya - Telp.(031)–5947572",
                "Jl. Menur No.120, Surabaya - Telp.(031)–5021635",
                "Jl. Pacarkeling No.15A, Surabaya - Telp.(031)-5035053",
                "Jl. Arief Rachman Hakim No.122, Surabaya - Telp.(031)–5999987",
                "Jl. Nginden Intan Barat B - Telp.(031)–5993211",
                "Jl. Ksatrian No.17, Surabaya - Telp.(031)–5681635",
                "Jl. Gadung No.1, Surabaya - Telp.(031)–843 8153",
                "Jl. Ngagel Jaya Utara 2AB, Surabaya - Telp.(031)–5042395",
                "Jl. Ketintang Madya I No.16, Surabaya - Telp.(031)–8291799",
                "Jl. Jend. A. Yani No.116, Surabaya - Telp.(031)–8292227",
                "Jl. Dukuh Pakis No.2, Surabaya - Telp. (031)–5617163",
                "Jl. Jend. A. Yani No.2–4, Surabaya - Telp.(031)–8281744",
                "Jl. Jemursari No.51–57, Surabaya - Telp.(031)–8437784",
                "Jl. Menganti Kedurus No.38–40, Surabaya - Telp.(031)–7663986",
                "Jl. Prof Dr. Mustopo No.68, Surabaya - Telp.(031)–5020079",
                "Jl. Prof. Dr. Moestopo No.31–33–35, Surabaya - Telp.(031)–5017975",
                "Jl. Serayu No.17, Surabaya",
                "Jl. Kapasari No.97–101, Surabaya - Telp.(031)–3764555",
                "Jl. Undaan Wetan No.40–44, Surabaya - Telp.(031)–5321256",
                "Jl. Raya Darmo No.90, Surabaya - Telp.(031)–5676253",
                "Jl. Diponegoro No.51 - Telp.(031)–5677562",
                "Jl. Demak No.443, Surabaya - Telp.(031)–3555821",
                "Jl. Kayoon No.1–3–5, Surabaya - Telp.(031)–5345426",
                "Jl. Diponegoro No.34, Surabaya - Telp.(031)–5678917",
                "Jl. Undaan Kulon No.19, Surabaya - Telp.(031)–5343806",
                "Jl. Golf No.1, Surabaya - Telp.(031)–5663539",
                "Jl. Raya Benowo No.5, Surabaya - Telp.(031)–7406293",
                "Jl. Satelit Indah II, Surabaya - Telp.(031)–7345333",
        };

        CustomListAdapter adapter=new CustomListAdapter(this, itemname, Description, imgid);
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String Slecteditem= itemname[+position];
                Toast.makeText(getApplicationContext(), Slecteditem, Toast.LENGTH_SHORT).show();

            }
        });

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
 }

}
