package id.ac.pens.student.it.dipta.onlyn;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.ac.pens.student.it.dipta.onlyn.models.Node;
import id.ac.pens.student.it.dipta.onlyn.utils.LocationService;

/**
 * Created by Dipta on 4/23/2016.
 */
public class NavigationFragment extends Fragment {


    @Bind(R.id.tawal)
    AutoCompleteTextView tawal;
    @Bind(R.id.ttujuan)
    AutoCompleteTextView ttujuan;
    @Bind(R.id.bcari)
    Button bcari;

    ArrayList<LatLng> locations = new ArrayList<>();

    LatLng awal, akhir;
    @Bind(R.id.gps)
    CheckBox gps;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.navigation_layout, container, false);
        ButterKnife.bind(this, view);

        tawal.setThreshold(3);
        tawal.setAdapter(new CustomAdapter(getActivity(), android.R.layout.simple_list_item_1));

        tawal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                awal = locations.get(position);
            }
        });

        ttujuan.setThreshold(3);
        ttujuan.setAdapter(new CustomAdapter(getActivity(), android.R.layout.simple_list_item_1));

        ttujuan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                akhir = locations.get(position);
            }
        });

        gps.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                tawal.setEnabled(!isChecked);
            }
        });

        return view;
    }

    private ArrayList<String> getAddressInfo(String locationName) {
        locations.clear();
        ArrayList<String> list = new ArrayList<>();
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        list.clear();
        try {
            List<Address> a = geocoder.getFromLocationName(locationName, 5);
            for (int i = 0; i < a.size(); i++) {
                String buff = "";
                for (int x = 0; x < a.get(i).getMaxAddressLineIndex(); x++) {
                    if (x > 0)
                        buff += ", ";
                    buff += a.get(i).getAddressLine(x);
                }
                list.add(buff);
                locations.add(new LatLng(a.get(i).getLatitude(), a.get(i).getLongitude()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Node n : DBAdapter.getInstance(getActivity()).getNodes(locationName)) {
            locations.add(new LatLng(n.getLat(), n.getLng()));
            list.add(n.getNode());
        }

        return list;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.bcari)
    public void onClick() {

        if (awal == null && !gps.isChecked()) {
            Toast.makeText(getActivity(), "Titik awal belum ditentukan", Toast.LENGTH_SHORT).show();
            return;
        }

        if (akhir == null) {
            Toast.makeText(getActivity(), "Tujuan belum ditentukan", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent intent = new Intent(getActivity(), MapsActivity.class);
        intent.putExtra("awal", awal);
        intent.putExtra("tujuan", akhir);
        intent.putExtra("gps", gps.isChecked());

        startActivity(intent);
    }

    class CustomAdapter extends ArrayAdapter implements Filterable {
        private ArrayList<String> resultList;

        public CustomAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        resultList = getAddressInfo(constraint.toString());
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }
}
