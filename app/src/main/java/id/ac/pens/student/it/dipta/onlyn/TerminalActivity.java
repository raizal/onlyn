package id.ac.pens.student.it.dipta.onlyn;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class TerminalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terminal2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ListView list;
        final String[] itemname ={
                "Terminal Purabaya (Bungurasih)",
                "Terminal Tambak Osowilangun",
                "Terminal Joyoboyo",
                "Terminal Balongsari",
                "Terminal Benowo",
                "Terminal Bratang",
                "Terminal Dukuh Kupang",
                "Terminal Kalimas Barat",
                "Terminal Kedung Cowek",
                "Terminal Kenjeran",
                "Terminal Keputih",
                "Terminal Manukan",
                "Terminal Menanggal"
        };

        Integer[] imgid={
                R.drawable.terminal,
                R.drawable.terminal,
                R.drawable.terminal,
                R.drawable.terminal,
                R.drawable.terminal,
                R.drawable.terminal,
                R.drawable.terminal,
                R.drawable.terminal,
                R.drawable.terminal,
                R.drawable.terminal,
                R.drawable.terminal,
                R.drawable.terminal,
                R.drawable.terminal,
        };

        final String[] Description ={
                "Jl. Letjen Sutoyo, Bungurasih, Waru, Sidoarjo, Jawa Timur",
                "Jl. Tambak Osowilangun, Tambak Osowilangun, Benowo, Surabaya, Jawa Timur",
                "Jl. Joyoboyo, Sawunggaling, Wonokromo, Surabaya, Jawa Timur",
                "Jl. Balongsari Tama, Balongsari, Tandes, Surabaya, Jawa Timur",
                "Jl. Benowo, Benowo, Pakal, Surabaya, Jawa Timur",
                "Jl. Manyar, Barata Jaya, Gubeng, Surabaya, Jawa Timur",
                "Jl. Dukuh Kupang 21, Pakis, Sawahan, Surabaya, Jawa Timur",
                "Jl. Raya Petekan, Perak Timur, Pabean Cantikan, Surabaya, Jawa Timur",
                "Jl. Tambak Wedi No.2, Kedung Cowek, Bulak, Surabaya, Jawa Timur",
                "Jl. Abdul Latief, Kenjeran, Bulak, Surabaya, Jawa Timur",
                "Jl. Keputih Tegal, Keputih, Sukolilo, Surabaya, Jawa Timur",
                "Jl. Raya Lontar, Lontar, Sambikerep, Surabaya, Jawa Timur",
                "Jl. Dukuh Menanggal, Menanggal, Gayungan, Surabaya, Jawa Timur",
        };

        CustomListAdapter adapter=new CustomListAdapter(this, itemname, Description, imgid);
            list=(ListView)findViewById(R.id.list);
            list.setAdapter(adapter);

            list.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    String Slecteditem= itemname[+position];
                    Toast.makeText(getApplicationContext(), Slecteditem, Toast.LENGTH_SHORT).show();

                }
            });
        }

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

