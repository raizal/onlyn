package id.ac.pens.student.it.dipta.onlyn.models;

import android.database.Cursor;

import id.ac.pens.student.it.dipta.onlyn.utils.Utils;

/**
 * Created by Dipta on 4/28/2016.
 */
public class Bus {
    private long _id;
    private String kode;
    private String trayek;
    private String detail;

    public static Bus fromCursor(Cursor cursor){
        return  new Bus(
                cursor.getLong(cursor.getColumnIndex("id_bus_kota")),
                cursor.getString(cursor.getColumnIndex("kode")),
                Utils.unescape(cursor.getString(cursor.getColumnIndex("trayek"))),
                Utils.unescape(cursor.getString(cursor.getColumnIndex("detail")))
        );
    }

    public Bus(long _id, String kode, String trayek, String detail) {
        this._id = _id;
        this.kode = kode;
        this.trayek = trayek;
        this.detail = detail;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getTrayek() {
        return trayek;
    }

    public void setTrayek(String trayek) {
        this.trayek = trayek;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
