package id.ac.pens.student.it.dipta.onlyn.models;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import id.ac.pens.student.it.dipta.onlyn.DBAdapter;

/**
 * Created by raizal.pregnanta on 28/04/2016.
 */
public class Node {
    private long _id;


    public Node(long _id, String node, double lat, double lng) {
        this._id = _id;
        this.node = node;
        this.lat = lat;
        this.lng = lng;
    }
    public Node(long _id) {
        this._id = _id;
    }

    private String node;

    private double lat;
    private double lng;


    public static Node fromCursor(Cursor c){
        Node node = new Node(
                c.getLong(c.getColumnIndex("id_node")),
                c.getString(c.getColumnIndex("node")),
                c.getDouble(c.getColumnIndex("lat")),
                c.getDouble(c.getColumnIndex("lng")));
        return node;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getNode() {
        return node;
    }

    private List<Edge> edgeList = new ArrayList<>();

    public void setNode(String node) {
        this.node = node;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public List<Edge> getEdgeList() {
        return edgeList;
    }

    public void setEdgeList(List<Edge> edgeList) {
        this.edgeList = edgeList;
    }
}
