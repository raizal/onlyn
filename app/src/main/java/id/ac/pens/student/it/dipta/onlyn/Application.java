package id.ac.pens.student.it.dipta.onlyn;

import android.content.Intent;

import id.ac.pens.student.it.dipta.onlyn.utils.LocationService;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by raizal.pregnanta on 25/07/2016.
 */
public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
//        startService(new Intent(this, LocationService.class));
    }

}
