package id.ac.pens.student.it.dipta.onlyn.utils;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
//import android.widget.Toast;

import java.util.List;

import id.ac.pens.student.it.dipta.onlyn.MapsActivity;
import id.ac.pens.student.it.dipta.onlyn.R;
import id.ac.pens.student.it.dipta.onlyn.models.Edge;

public class LocationService extends Service {

    public static int NOTIF_ID = 123312;
    public static int RADIUS = 50;
    public static boolean alreadyInsideRadius = false;
    private static String TAG = "LOCATION SERVICE";
    private static List<Edge> rute;
    public GpsStatus.Listener mGPSStatusListener = new GpsStatus.Listener() {
        public void onGpsStatusChanged(int event) {
            switch (event) {
                case GpsStatus.GPS_EVENT_STARTED:
                    System.out.println("TAG - GPS searching: ");
                    break;
                case GpsStatus.GPS_EVENT_STOPPED:
                    System.out.println("TAG - GPS Stopped");
                    break;
                case GpsStatus.GPS_EVENT_FIRST_FIX:

                /*
                 * GPS_EVENT_FIRST_FIX Event is called when GPS is locked
                 */
//                    Toast.makeText(LocationService.this, "GPS_LOCKED", Toast.LENGTH_SHORT).show();
//                    if (ActivityCompat.checkSelfPermission(LocationService.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(LocationService.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                        return;
//                    }
//                    Location gpslocation = mLocationManager
//                            .getLastKnownLocation(LocationManager.GPS_PROVIDER);
//
//                    if (gpslocation != null) {
//                        Toast.makeText(LocationService.this, "GPS Info:" + gpslocation.getLatitude() + ":" + gpslocation.getLongitude(), Toast.LENGTH_SHORT).show();
//                        System.out.println("GPS Info:" + gpslocation.getLatitude() + ":" + gpslocation.getLongitude());
//
//                    /*
//                     * Removing the GPS status listener once GPS is locked
//                     */
//                        mLocationManager.removeGpsStatusListener(mGPSStatusListener);
//                    }

                    break;
                case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
//                    System.out.println("TAG - GPS_EVENT_SATELLITE_STATUS");
                    break;
            }
        }
    };
    private LocationManager mLocationManager;

    public LocationService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        try {

            mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mLocationManager.addGpsStatusListener(mGPSStatusListener);
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 1, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    Log.d(TAG, "LOCATION CHANGED : " + location.getLatitude() + ";" + location.getLongitude());
//                    Toast.makeText(LocationService.this, "LOCATION CHANGED : " + location.getLatitude() + ";" + location.getLongitude(), Toast.LENGTH_SHORT).show();
                    if (Utils.isNavigating(LocationService.this)) {
                        if (rute == null || (rute == null && Utils.currentNavigationIndex(LocationService.this) == 0)) {
                            rute = Utils.routes(LocationService.this);
                        }

                        if (rute == null || rute.size() == 0)
                            return;

                        int index = (Utils.currentNavigationIndex(LocationService.this));
                        if(index==rute.size()) {
                            index -= 1;
                            Utils.setCurrentNavigationIndex(LocationService.this,index);
                        }
                        Edge current = rute.get(index);

                        if (current == null)
                            return;

                        //in meter
                        float distance = ((float) DistanceCalculator.distance(current.getLat(), current.getLng(), location.getLatitude(), location.getLongitude(), "K")) * 1000;

                        boolean pindah = false;

                        if (distance <= RADIUS) {
                            //notify user
                            NotificationCompat.Builder mBuilder =
                                    new NotificationCompat.Builder(LocationService.this)
                                            .setSmallIcon(R.mipmap.ic_launcher)
                                            .setContentTitle(getResources().getString(R.string.app_name))
                                            .setAutoCancel(false)
                                            .setOngoing(true);

                            if (!alreadyInsideRadius) {
                                alreadyInsideRadius = true;
                                mBuilder.setPriority(Notification.PRIORITY_HIGH)
                                        .setVibrate(new long[]{100, 100, 300, 100,500})
                                        .setDefaults(Notification.DEFAULT_SOUND|Notification.DEFAULT_LIGHTS);
                            }

                            if (current.getIdAsal() == rute.get(rute.size()-1).getIdAsal()) {
                                mBuilder.setContentText("Sampai di titik pemberhentian terakhir").setOngoing(false).setAutoCancel(true);
                                Utils.setNavigatingStatus(LocationService.this, false);
                                stopSelf();
                            } else {
                                NotificationCompat.InboxStyle inboxStyle =
                                        new NotificationCompat.InboxStyle();
                                inboxStyle.addLine("Naik " + current.getLyn());
                                mBuilder.setStyle(inboxStyle).setContentText("Naik " + current.getLyn());
                            }

                            Intent i = new Intent(LocationService.this, MapsActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                            PendingIntent pendingIntent = PendingIntent.getActivity(LocationService.this, NOTIF_ID, i, PendingIntent.FLAG_UPDATE_CURRENT);
                            mBuilder.setContentIntent(pendingIntent);

                            notificationManager.notify(NOTIF_ID, mBuilder.build());
                        } else {
                            if (alreadyInsideRadius) {
                                alreadyInsideRadius = false;
                                {
                                    //CHANGE INDEX OF NEXT NODE
                                    int i = index;
                                    while (i <= rute.size()) {
                                        if (i < rute.size() && !current.getLyn().equals(rute.get(i).getLyn())) {
                                            index = i;
                                            break;
                                        } else if (rute.size() == i) {
                                            index = i;
                                        }
                                        i++;
                                    }
                                    Utils.setCurrentNavigationIndex(LocationService.this, index);
                                }
                            }

                            if (current != rute.get(index)) {
                                current = rute.get(index);
                                distance = ((float) DistanceCalculator.distance(current.getLat(), current.getLng(), location.getLatitude(), location.getLongitude(), "K")) * 1000;
                            }
                            NotificationCompat.Builder
                                    mBuilder =
                                    new NotificationCompat.Builder(LocationService.this)
                                            .setSmallIcon(R.mipmap.ic_launcher)
                                            .setContentTitle(getResources().getString(R.string.app_name))
                                            .setAutoCancel(false)
                                            .setOngoing(true)
                                            .setContentText(distance + " meter lagi");

                            NotificationCompat.InboxStyle inboxStyle =
                                    new NotificationCompat.InboxStyle();
                            inboxStyle.addLine(distance + " meter lagi");
                            if (current.getIdAsal() == rute.get(rute.size()-1).getIdAsal()) {
                                inboxStyle.addLine("Titik permberhentian terakhir");
                            }else {
                                inboxStyle.addLine(Utils.getNavigationText(LocationService.this).get(index));
                            }

                            mBuilder.setStyle(inboxStyle);

                            Intent i = new Intent(LocationService.this, MapsActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                            PendingIntent pendingIntent = PendingIntent.getActivity(LocationService.this, NOTIF_ID, i, PendingIntent.FLAG_UPDATE_CURRENT);
                            mBuilder.setContentIntent(pendingIntent);
                            notificationManager.notify(NOTIF_ID, mBuilder.build());
                        }
                    }
                    Utils.setLastLocation(LocationService.this, location);
                    Intent intent = new Intent();
                    intent.setAction("LOCATION UPDATE");
                    sendBroadcast(intent);
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {
                    //Log.d(TAG, "STATUS CHANGED : " + provider + "\nstatus : " + status);
                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        init();
        return START_STICKY;
    }
}
