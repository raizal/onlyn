package id.ac.pens.student.it.dipta.onlyn;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import id.ac.pens.student.it.dipta.onlyn.models.Taxi;

/**
 * Created by Dipta on 4/23/2016.
 */
public class InformationTaxiFragment extends Fragment {

    @Bind(R.id.recyclerview)
    RecyclerView recyclerview;

    List<Taxi> data = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.information_section_layout, container, false);
        ButterKnife.bind(this, view);
        data.addAll(DBAdapter.getInstance(getActivity()).getTaxi());
        recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerview.setAdapter(new AdapterBus());
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    static class BasicViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.text)
        TextView text;
        @Bind(R.id.trayek)
        TextView trayek;
        @Bind(R.id.desc)
        TextView desc;
        @Bind(R.id.more)
        RelativeLayout more;
        View view;

        BasicViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
            more.setVisibility(View.GONE);
        }
    }

    class AdapterBus extends RecyclerView.Adapter<BasicViewHolder> {

        @Override
        public BasicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new BasicViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_informasi, parent, false));
        }

        @Override
        public void onBindViewHolder(final BasicViewHolder holder, int position) {
            Taxi taxi = data.get(position);
            holder.text.setText(taxi.getNama());
            holder.trayek.setText(taxi.getTelp());
        }

        @Override
        public int getItemCount() {
            return data.size();
        }
    }
}