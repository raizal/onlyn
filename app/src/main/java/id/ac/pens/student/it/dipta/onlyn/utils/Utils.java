package id.ac.pens.student.it.dipta.onlyn.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.view.Menu;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import java.util.ArrayList;
import java.util.List;

import id.ac.pens.student.it.dipta.onlyn.models.Edge;
import id.ac.pens.student.it.dipta.onlyn.models.Node;

/**
 * Created by Dipta on 6/7/2016.
 */
public class Utils {

    public static String unescape(String text){
        return text.replaceAll("\\\\n","\\\n");
    }
    public static Bitmap scaleImage(Resources res, int id, int lessSideSize) {
        Bitmap b = null;
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;

        BitmapFactory.decodeResource(res, id, o);

        float sc = 0.0f;
        int scale = 1;
        // if image height is greater than width
        if (o.outHeight > o.outWidth) {
            sc = o.outHeight / lessSideSize;
            scale = Math.round(sc);
        }
        // if image width is greater than height
        else {
            sc = o.outWidth / lessSideSize;
            scale = Math.round(sc);
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        b = BitmapFactory.decodeResource(res, id, o2);
        return b;
    }

    public static boolean isNavigating(Context context){
        boolean status = context.getSharedPreferences("navigasi",Context.MODE_PRIVATE).getBoolean("navigating",false);
        return status;
    }

    public static boolean setNavigatingStatus(Context context,boolean status){
        context.getSharedPreferences("navigasi",Context.MODE_PRIVATE).edit().putBoolean("navigating",status).apply();
        return status;
    }

    public static int currentNavigationIndex(Context context){
        return context.getSharedPreferences("navigasi",Context.MODE_PRIVATE).getInt("index",-1);
    }

    public static void setCurrentNavigationIndex(Context context,int index){
        context.getSharedPreferences("navigasi",Context.MODE_PRIVATE).edit().putInt("index",index).apply();
    }

    public static List<Edge> routes(Context context){
        String str = context.getSharedPreferences("navigasi",Context.MODE_PRIVATE).getString("route","");
        if(str.equals(""))
            return new ArrayList<>();

        return new Gson().fromJson(str,new TypeToken<List<Edge>>(){}.getType());
    }

    public static void setRoutes(Context context,List<Edge> routes){
        context.getSharedPreferences("navigasi",Context.MODE_PRIVATE).edit().putString("route",new Gson().toJson(routes)).apply();
    }

    public static void setStartPoint(Context context, LatLng point){
        context.getSharedPreferences("navigasi",Context.MODE_PRIVATE).edit().putString("start",new Gson().toJson(point)).apply();
    }

    public static void setLastPoint(Context context, LatLng point){
        context.getSharedPreferences("navigasi",Context.MODE_PRIVATE).edit().putString("last",new Gson().toJson(point)).apply();
    }

    public static LatLng getStartPoint(Context context){
        String str =  context.getSharedPreferences("navigasi",Context.MODE_PRIVATE).getString("start","");
        if(str.equals(""))
            return null;
        return new Gson().fromJson(str,LatLng.class);
    }

    public static void setNavigationText(Context context, List<String> texts){
        context.getSharedPreferences("navigasi",Context.MODE_PRIVATE).edit().putString("text",new Gson().toJson(texts)).apply();
    }

    public static List<String> getNavigationText(Context context){
        String str =  context.getSharedPreferences("navigasi",Context.MODE_PRIVATE).getString("text","");
        if(str.equals(""))
            return null;
        return new Gson().fromJson(str,new TypeToken<List<String>>(){}.getType());
    }

    public static LatLng getLastPoint(Context context){
        String str =  context.getSharedPreferences("navigasi",Context.MODE_PRIVATE).getString("last","");
        if(str.equals(""))
            return null;
        return new Gson().fromJson(str,LatLng.class);
    }

    public static void setLastLocation(Context context,Location location){
        LatLng lastLocation = new LatLng(location.getLatitude(),location.getLongitude());
        context.getSharedPreferences("navigasi",Context.MODE_PRIVATE).edit().putString("lastlocation",new Gson().toJson(lastLocation)).apply();
    }

    public static void setLastLocation(Context context,LatLng lastLocation){
        context.getSharedPreferences("navigasi",Context.MODE_PRIVATE).edit().putString("lastlocation",new Gson().toJson(lastLocation)).apply();
    }

    public static LatLng getLastLocation(Context context){
        String str =  context.getSharedPreferences("navigasi",Context.MODE_PRIVATE).getString("lastlocation","");
        if(str.equals(""))
            return null;
        return new Gson().fromJson(str,LatLng.class);
    }

    public static void iconicsMenu(Context context,Menu menu,int id,String icon){
        menu.findItem(id).setIcon(
                new IconicsDrawable(context,icon)
                        .color(Color.WHITE)
                        .actionBar()
        );
    }
}