package id.ac.pens.student.it.dipta.onlyn;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Dipta on 4/23/2016.
 */
public class PublicFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.public_layout, container, false);

        Button inten1 = (Button) rootView.findViewById(R.id.btnterminal);
        inten1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Pass the context and the Activity class you need to open from the Fragment Class, to the Intent
                Intent intent = new Intent(getActivity(), TerminalActivity.class);
                startActivity(intent);
            }
        });

        Button inten2 = (Button) rootView.findViewById(R.id.btnpelabuhan);
        inten2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Pass the context and the Activity class you need to open from the Fragment Class, to the Intent
                Intent intent = new Intent(getActivity(), PelabuhanActivity.class);
                startActivity(intent);
            }
        });

        Button inten3 = (Button) rootView.findViewById(R.id.btnbandara);
        inten3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Pass the context and the Activity class you need to open from the Fragment Class, to the Intent
                Intent intent = new Intent(getActivity(), BandaraActivity.class);
                startActivity(intent);
            }
        });

        Button inten4 = (Button) rootView.findViewById(R.id.btnstasiun);
        inten4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Pass the context and the Activity class you need to open from the Fragment Class, to the Intent
                Intent intent = new Intent(getActivity(), StasiunActivity.class);
                startActivity(intent);
            }
        });

        Button inten5 = (Button) rootView.findViewById(R.id.btnpasar);
        inten5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Pass the context and the Activity class you need to open from the Fragment Class, to the Intent
                Intent intent = new Intent(getActivity(), PasarActivity.class);
                startActivity(intent);
            }
        });

        Button inten6 = (Button) rootView.findViewById(R.id.btnrs);
        inten6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Pass the context and the Activity class you need to open from the Fragment Class, to the Intent
                Intent intent = new Intent(getActivity(), RumahsakitActivity.class);
                startActivity(intent);
            }
        });

        return rootView;
    }
}