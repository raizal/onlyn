package id.ac.pens.student.it.dipta.onlyn.models;

import android.database.Cursor;

import java.util.Comparator;

/**
 * Created by raizal.pregnanta on 28/04/2016.
 */
public class Edge implements Comparable<Edge> {
    private long _id;
    private long idAsal, idAkhir;
    private Edge parent;
    private String lyn;
    private int cost;
    private double lat, lng;

    private Node node;

    private double heuristic;

    public Edge() {

    }

    public Edge(long _id, double jarak, long asal, long akhir, String lyn) {
        this._id = _id;
        this.heuristic = jarak;
        idAsal = asal;
        idAkhir = akhir;
        this.lyn = lyn;
    }

    public static Edge fromCursor(Cursor c) {
        Edge e = new Edge(
                c.getLong(c.getColumnIndex("id_edge")),
                c.getLong(c.getColumnIndex("id_edge")),
                c.getLong(c.getColumnIndex("id_node")),
                c.getLong(c.getColumnIndex("id_node_tujuan")),
                c.getString(c.getColumnIndex("lyn"))
        );

        if (c.getColumnIndex("lat") >= 0) {
            e.setLat(c.getDouble(c.getColumnIndex("lat")));
        }

        if (c.getColumnIndex("lng") >= 0) {
            e.setLng(c.getDouble(c.getColumnIndex("lng")));
        }

        if (!e.getLyn().toLowerCase().equals("jalan kaki") && !e.getLyn().toLowerCase().equals("bus")) {
            e.setLyn("Lyn "+e.getLyn());
        }

        return e;
    }

    public double getHeuristic() {
        return heuristic;
    }

    public void setHeuristic(double heuristic) {
        this.heuristic = heuristic;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }


    public long getIdAsal() {
        return idAsal;
    }

    public void setIdAsal(long idAsal) {
        this.idAsal = idAsal;
    }

    public long getIdAkhir() {
        return idAkhir;
    }

    public void setIdAkhir(long idAkhir) {
        this.idAkhir = idAkhir;
    }

    public Edge getParent() {
        return parent;
    }

    public void setParent(Edge parent) {
        this.parent = parent;
    }

    public String getLyn() {
        return lyn;
    }

    public void setLyn(String lyn) {
        this.lyn = lyn;
    }

    public double getF() {
        return cost + heuristic;
//        return heuristic;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }


    public boolean equals(Edge o) {
        return (o.getLat()==getLat() && o.getLng() == getLng());
    }

    public boolean equals(Node o) {
        return (o.getLat()==getLat() && o.getLng() == getLng());
    }

    @Override
    public int compareTo(Edge another) {
        int result = (int)((getF() - another.getF())*1000);
        return result;
    }

    @Override
    public String toString() {
        return lyn+"["+getF()+"]:"+idAsal+"->"+idAkhir;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }
}
