package id.ac.pens.student.it.dipta.onlyn.models;

import java.util.Iterator;
import java.util.PriorityQueue;

/**
 * Created by raizal.pregnanta on 14/07/2016.
 */
public class SortedList extends PriorityQueue<Edge> {

    public Edge pop(Edge p) {
        Iterator<Edge> i = iterator();
        while (i.hasNext()) {
            Edge current = i.next();
            if (current.getLat() == p.getLat() && current.getLng() == p.getLng()) {
                remove(current);
                return current;
            }
        }
        return null;
    }

    public Edge get(double lat, double lng) {
        Iterator<Edge> i = iterator();

        while (i.hasNext()) {
            Edge current = i.next();
            if (current.getLat() == lat && current.getLng() == lng) {
                return current;
            }
        }
        return null;
    }

    public Edge pop(double lat, double lng) {
        Iterator<Edge> i = iterator();

        while (i.hasNext()) {
            Edge current = i.next();
            if (current.getLat() == lat && current.getLng() == lng) {
                remove(current);
                return current;
            }
        }
        return null;
    }
};
