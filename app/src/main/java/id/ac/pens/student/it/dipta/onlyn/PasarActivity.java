package id.ac.pens.student.it.dipta.onlyn;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class PasarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ListView list;
        final String[] itemname ={
                "Pasar Turi",
                "Pusat Grosir Surabaya",
                "Pasar Atom",
                "Gubeng Kertajaya",
                "Bratang",
                "Keputih",
                "Kendangsari",
                "Pacar Keling",
                "Ambengan Batu",
                "Kelapa",
                "Sutorejo",
                "Gubeng Masjid",
                "Tambah Rejo",
                "Indrakila",
                "Tenggilis",
                "Asemrowo",
                "Babadan Baru",
                "Bibis",
                "Balongsari",
                "Dupak Rukun",
                "Dupak Banderejo",
                "Krembangan",
                "Pabean",
                "Pegirikan",
                "Manukan Kulon",
                "Simo",
                "Simomulya Baru",
                "Tembok Dukuh",
                "Wonokusumo",
                "Koblen",
                "Jl. Gresik",
                "Pesapen",
                "Kepatihan",
                "Jembatan Merah",
                "Dupak bangunrejo",
                "Banjar Sugihan",
                "Sukodono",
                "Ampel",
                "Bangkingan",
                "Bendul Merisi",
                "Wonokromo Lama",
                "Gayungsari",
                "Blauran Baru",
                "Dukuh Kupang",
                "Genteng Baru",
                "Karang Pilang",
                "Lakarsantri",
                "Hewan karangpilang",
                "Kembang",
                "Kedungsari",
                "Keputran Utara",
                "Keputran Selatan",
                "Pasar Pahing",
                "Kapasari Baru",
                "Aswotomo",
                "Pucang Anom",
        };

        Integer[] imgid={
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
                R.drawable.pasar,
        };

        final String[] DescriptionPasar ={

                "Jl.Semarang",
                "Jl.Raya Dupak",
                "Jl.Jalan Bunguran No.45",
                "Jl.Kertajaya",
                "Jl.Bratang Binangun",
                "Jl.Keputih",
                "Jl.Kendangsari",
                "Jl.Pacar Keling",
                "Jl.Ambengan Batu",
                "Jl.Kelapa",
                "Jl.Sutorejo",
                "Jl.Gubeng Masjid",
                "Jl.Kapas Krampung",
                "Jl.Indrakila",
                "Jl.Tenggilis",
                "Jl.Asemrowo Makam",
                "Jl.Kebalen Timur",
                "Jl.Waspada",
                "Jl.Balongsari",
                "Jl.Dupak Rukun",
                "Jl.Dupak Banderjo I",
                "Jl.Krembangan",
                "Jl.Songoyudan 77",
                "Jl.Nyamplungan",
                "Jl.Raya Manukan Lor",
                "Jl.Simo",
                "Jl.Simomulyo Ngesang",
                "Jl.Kranggan 120",
                "Jl.Wonokusumo Wetan Gg.I",
                "Jl.Raden Saleh",
                "Jl.Gresik",
                "Jl.Pesapen",
                "Jl.Kramat gantung",
                "Jl.Veteran",
                "Jl.Dupak Banderejo",
                "Jl.Banjar Sugihan",
                "Jl.Sukodono",
                "Jl.Ampel",
                "Jl.Bangkingan",
                "Jl.Bendul Merisi",
                "Jl.raya Wonokromo",
                "Jl.Gayungan",
                "Jl.Kranggan",
                "Jl.Dukuh Kupang barat",
                "Jl.genteng Besar 62",
                "Jl.Raya Mastrip",
                "Jl.Lakarsantri",
                "Jl.Kolong Marinir",
                "Jl.Pasar Kembang",
                "Jl.Kedungsari",
                "Jl.Keputran 12",
                "Jl.Keputran",
                "Jl.Kali Rungkut",
                "Jl.Kapasan",
                "Jl.Sidodadi 183",
                "Jl.Pucang Anom",

        };


        CustomListAdapter adapter=new CustomListAdapter(this, itemname, DescriptionPasar, imgid);
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String Slecteditem= itemname[+position];
                Toast.makeText(getApplicationContext(), Slecteditem, Toast.LENGTH_SHORT).show();

            }
        });
    }
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
//    }

}
