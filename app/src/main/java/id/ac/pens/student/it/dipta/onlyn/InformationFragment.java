package id.ac.pens.student.it.dipta.onlyn;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Dipta on 5/2/2016.
 */
public class InformationFragment extends Fragment {
    @Bind(R.id.viewpager)
    ViewPager viewpager;
    @Bind(R.id.tabs)
    TabLayout tabs;

    private List<Fragment> pages = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.information_layout, container, false);
        ButterKnife.bind(this, view);

        pages.add(new InformationLynFragment());
        pages.add(new InformationBusFragment());
        pages.add(new InformationTaxiFragment());
        viewpager.setOnTouchListener(mSuppressInterceptListener);
        viewpager.setAdapter(new Adapter());
        tabs.setupWithViewPager(viewpager);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    private View.OnTouchListener mSuppressInterceptListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (
                    event.getAction() == MotionEvent.ACTION_DOWN &&
                            v instanceof ViewGroup
                    ) {
                ((ViewGroup) v).requestDisallowInterceptTouchEvent(true);
            }
            return false;
        }
    };


    private class Adapter extends FragmentPagerAdapter {

        public Adapter() {
            super(getChildFragmentManager());
        }

        @Override
        public Fragment getItem(int position) {
            return pages.get(position);
        }

        @Override
        public int getCount() {
            return pages.size();
        }

        private String[] titles = new String[]{"Lyn", "Bus", "Taxi"};

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
    }

    public Fragment getCurrent(){
        return pages.get(viewpager.getCurrentItem());
    }
}