package id.ac.pens.student.it.dipta.onlyn;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

import id.ac.pens.student.it.dipta.onlyn.models.Bus;
import id.ac.pens.student.it.dipta.onlyn.models.Edge;
import id.ac.pens.student.it.dipta.onlyn.models.Lyn;
import id.ac.pens.student.it.dipta.onlyn.models.Node;
import id.ac.pens.student.it.dipta.onlyn.models.Taxi;

/**
 * Created by Dipta on 4/28/2016.
 */
public class DBAdapter extends SQLiteAssetHelper {
    private static final String DATABASE_NAME = "takeme";
    private static final int DATABASE_VERSION = 1;

    private static DBAdapter instance;

    private Context context;

    public static DBAdapter getInstance(Context context) {
        if(instance==null)
            instance = new DBAdapter(context);
        return instance;
    }

    public DBAdapter(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public List<Bus> getBus(){
        List<Bus> data = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("select * from tbl_bus_kota",null);

        if(c.moveToFirst()){
            do{
                data.add(Bus.fromCursor(c));
            }while(c.moveToNext());
        }
        c.close();
        db.close();
        return data;
    }

    public List<Lyn> getLyn(String keyword){
        List<Lyn> data = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("select * from tbl_lyn where detail like '%"+keyword+"%'",null);

        if(c.moveToFirst()){
            do{
                data.add(Lyn.fromCursor(c));
            }while(c.moveToNext());
        }

        c.close();
        db.close();
        return data;
    }

    public List<Taxi> getTaxi(){
        List<Taxi> data = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery("select * from tbl_taxi",null);

        if(c.moveToFirst()){
            do{
                data.add(Taxi.fromCursor(c));
            }while(c.moveToNext());
        }
        c.close();
        db.close();
        return data;
    }

//    public List<Edge> getEdges(long idLyn){
//        List<Edge> data = new ArrayList<>();
//        String query = "select (select count(*) from tbl_edge where id_node=e.id_node and lyn=e.lyn) as child," +
//                "e.id_edge," +
//                "e.jarak,e.id_node," +
//                "e.id_node_tujuan, n1.node as node1,n1.lat as lat1,n1.lng as lng1 ,n2.node as node2,n2.lat as lat2,n2.lng as lng2, tbl_lyn.* " +
//                "from tbl_edge e,tbl_node n1,tbl_node n2,tbl_lyn where e.id_node=n1.id_node  and  e.id_node_tujuan=n2.id_node and tbl_lyn.lyn = e.lyn and tbl_lyn.id_lyn=? " +
//                "order by child";
//        SQLiteDatabase db = getWritableDatabase();
//        Cursor c = db.rawQuery(query,new String[]{String.valueOf(idLyn)});
//
//        if(c.moveToFirst()){
//            do{
//                data.add(Edge.fromCursor(c));
//            }while(c.moveToNext());
//        }
//
//        c.close();
//        db.close();
//
//        List<Edge> parentEdge = new ArrayList<>();
//        List<Edge> dummy = new ArrayList<>();
//        dummy.addAll(data);
//
//        for(Edge e : data){
//
//            for(Edge x:dummy){
//                if(e==x)
//                    continue;
//                if(e.getAkhir().get_id()==x.getAsal().get_id() && e.getAsal().get_id()!=x.getAkhir().get_id()){
//
//                    x.setPrev(e);
//                    e.setNext(x);
//                    break;
//                }
//            }
//
//        }
//
//        for (Edge e:data){
//            if( //(e.getLyn().getBerangkat()==e.getAsal().get_id() || e.getLyn().getKembali()==e.getAsal().get_id()) &&
//                    e.getPrev()==null && e.getChild()==1)
//                parentEdge.add(e);
//        }
//        Log.d("DEBUG EDGE",data.toString());
//
//        return parentEdge;
//    }

    public List<Edge> getEdges(){
        List<Edge> data = new ArrayList<>();
        String query = "select a.*,b.lat,b.lng from tbl_edge a,tbl_node b where a.id_node_tujuan = b.id_node";
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery(query,null);

        if(c.moveToFirst()){
            do{
                data.add(Edge.fromCursor(c));
            }while(c.moveToNext());
        }

        c.close();
        db.close();

        return data;
    }

    public List<Edge> getEdges(long idNode){
        List<Edge> data = new ArrayList<>();
        String query = "select a.*,b.lat,b.lng from tbl_edge a,tbl_node b where a.id_node = b.id_node and b.id_node="+idNode;
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery(query,null);

        if(c.moveToFirst()){
            do{
                data.add(Edge.fromCursor(c));
            }while(c.moveToNext());
        }

        c.close();
        db.close();

        return data;
    }

    public List<Node> getNodes(String keyword){
        List<Node> data = new ArrayList<>();
        String query = "select * from tbl_node where LOWER(node) like \""+keyword.toLowerCase()+"%\" order by id_node limit 5";
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery(query,null);

        if(c.moveToFirst()){
            do{
                data.add(Node.fromCursor(c));
            }while(c.moveToNext());
        }

        c.close();
        db.close();

        return data;
    }

    public List<Node> getNodes(){
        List<Node> data = new ArrayList<>();
        String query = "select * from tbl_node order by id_node";
        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery(query,null);

        if(c.moveToFirst()){
            do{
                data.add(Node.fromCursor(c));
            }while(c.moveToNext());
        }

        c.close();
        db.close();

        return data;
    }

    public Node getNearest(double lat,double lng){
        SQLiteDatabase db  = getWritableDatabase();

        Cursor c = db.rawQuery("select * ,( abs(lat - ("+lat+")) + abs(lng - ("+lng+"))) b from tbl_node order by b asc limit 1",null);

        if(c.moveToFirst()){
            return Node.fromCursor(c);
        }
        return null;
    }
}