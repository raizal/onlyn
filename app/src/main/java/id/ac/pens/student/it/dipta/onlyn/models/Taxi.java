package id.ac.pens.student.it.dipta.onlyn.models;

import android.database.Cursor;

/**
 * Created by Dipta on 4/28/2016.
 */
public class Taxi {

    private long _id;
    private String nama;
    private String telp;

    public static Taxi fromCursor(Cursor cursor){
        return new Taxi(
                cursor.getLong(cursor.getColumnIndex("id_taxi")),
                cursor.getString(cursor.getColumnIndex("nama_taxi")),
                cursor.getString(cursor.getColumnIndex("nomor_telpon"))
        );
    }

    public Taxi(long _id, String nama, String telp) {
        this._id = _id;
        this.nama = nama;
        this.telp = telp;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }
}
