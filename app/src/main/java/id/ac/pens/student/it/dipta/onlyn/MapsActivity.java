package id.ac.pens.student.it.dipta.onlyn;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.RoutingListener;
import com.flipboard.bottomsheet.BottomSheetLayout;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mikepenz.material_design_iconic_typeface_library.MaterialDesignIconic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.ac.pens.student.it.dipta.onlyn.models.Edge;
import id.ac.pens.student.it.dipta.onlyn.models.Lyn;
import id.ac.pens.student.it.dipta.onlyn.models.Node;
import id.ac.pens.student.it.dipta.onlyn.models.SortedList;
import id.ac.pens.student.it.dipta.onlyn.utils.DistanceCalculator;
import id.ac.pens.student.it.dipta.onlyn.utils.LocationService;
import id.ac.pens.student.it.dipta.onlyn.utils.Utils;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    List<Node> nodeList;
    @Bind(R.id.navigasi)
    Button navigasi;
    @Bind(R.id.bottomsheet)
    BottomSheetLayout bottomsheet;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.astar)
    Button astar;
    @Bind(R.id.astarcustom)
    Button astarcustom;

    private GoogleApiClient mGoogleApiClient;

    private boolean astartCustom = true;

    private GoogleMap map;
    private LatLng awal, tujuan;
    private Node lastPoint;
    private Node startPoint;
    private ArrayList<Polyline> polylines = new ArrayList<>();

    private FragmentRute fragmentRute = new FragmentRute();

    private Bitmap userIcon;

    private HashMap<String, Integer> lynColor = new HashMap<>();
    private boolean killed = false;
    private SortedList openlist = new SortedList();

    private List<Edge> hasil = new ArrayList<>();

    private List<String> textNavigasi = new ArrayList<>();

    private int navigationIndex = 0;

    private LocationManager locationManager;

    private Marker userMarker;

    private LinkedHashSet<Edge> closed = new LinkedHashSet<Edge>() {
        @Override
        public boolean contains(Object o) {
            Edge e = (Edge) o;
            Iterator<Edge> i = iterator();
            while (i.hasNext()) {
                Edge p = i.next();
                if (e.getLat() == p.getLat() && e.getLng() == p.getLng()) {
                    return true;
                }
            }

            return false;
        }
    };

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            Toast.makeText(MapsActivity.this, "BROADCAST RECEIVER", Toast.LENGTH_SHORT).show();
//            if(context!=null && intent!=null && intent.getAction().equals("LOCATION UPDATE"))
            {
                LatLng current = Utils.getLastLocation(MapsActivity.this);
                if (userMarker != null && current != null) {
                    userMarker.setPosition(current);
//                    Toast.makeText(MapsActivity.this, "BROADCAST RECEIVER : MARKER UPDATE", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter("LOCATION UPDATE");
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);

        userIcon = Utils.scaleImage(getResources(), R.drawable.user, 24);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("GPS belum Anda nyalakan. Silahkan nyalakan terlebih dahulu")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(intent, 1909);
                        }
                    }).show();
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        if (ActivityCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                                mGoogleApiClient);

                        if (mLastLocation != null) {
                            Utils.setLastLocation(MapsActivity.this, mLastLocation);

                            if (!Utils.isNavigating(MapsActivity.this) && getIntent().getExtras().getBoolean("gps")) {
                                navigasi.setVisibility(View.GONE);
                                awal = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                                tujuan = (LatLng) getIntent().getExtras().get("tujuan");

                                startPoint = DBAdapter.getInstance(MapsActivity.this).getNearest(awal.latitude, awal.longitude);
                                lastPoint = DBAdapter.getInstance(MapsActivity.this).getNearest(tujuan.latitude, tujuan.longitude);

                                nodeList = DBAdapter.getInstance(MapsActivity.this).getNodes();

                                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                                        .findFragmentById(R.id.map);
                                mapFragment.getMapAsync(MapsActivity.this);
                            }

                        }

                        mGoogleApiClient.disconnect();

                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .addApi(LocationServices.API)
                .build();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!Utils.isNavigating(this) && getIntent().getExtras().getBoolean("gps")) {
            return;
        }
        if (Utils.isNavigating(this)) {
            awal = Utils.getStartPoint(this);
            tujuan = Utils.getLastPoint(this);
            hasil = Utils.routes(this);
            textNavigasi = Utils.getNavigationText(this);
            fragmentRute.setData(textNavigasi);
            navigasi.setVisibility(View.GONE);
            navigationIndex = Utils.currentNavigationIndex(this);
        } else {
            navigasi.setVisibility(View.GONE);
            awal = (LatLng) getIntent().getExtras().get("awal");
            tujuan = (LatLng) getIntent().getExtras().get("tujuan");
        }

        startPoint = DBAdapter.getInstance(this).getNearest(awal.latitude, awal.longitude);
        lastPoint = DBAdapter.getInstance(this).getNearest(tujuan.latitude, tujuan.longitude);

        nodeList = DBAdapter.getInstance(MapsActivity.this).getNodes();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(MapsActivity.this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                Context context = getApplicationContext(); //or getActivity(), YourActivity.this, etc.

                LinearLayout info = new LinearLayout(context);
                info.setOrientation(LinearLayout.VERTICAL);

                TextView title = new TextView(context);
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.CENTER);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());

                TextView snippet = new TextView(context);
                snippet.setTextColor(Color.GRAY);
                snippet.setText(marker.getSnippet());

                info.addView(title);
                info.addView(snippet);

                return info;
            }
        });

        map.getUiSettings().setZoomControlsEnabled(true);
        drawInitialLocation();

        Bitmap icon = Utils.scaleImage(getResources(), R.drawable.busstop, 8);

        LatLng center = new LatLng(awal.latitude - (awal.latitude - tujuan.latitude) / 2, awal.longitude - (awal.longitude - tujuan.longitude) / 2);

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(center, 13f));

        if (!Utils.isNavigating(this)) {
            for (Node n : nodeList) {
                List<Edge> edges = DBAdapter.getInstance(MapsActivity.this).getEdges(n.get_id());
                String lyn="";
                for(Edge e:edges){
                    lyn+=e.getLyn()+"\n";
                }
                map.addMarker(new MarkerOptions().position(new LatLng(n.getLat(), n.getLng())).title(n.getNode()).snippet(lyn).icon(BitmapDescriptorFactory.fromBitmap(icon)));
            }
//            new AsyncTask<Void, Void, Void>() {
//
//                @Override
//                protected Void doInBackground(Void... params) {
//                    route();
//                    return null;
//                }
//            }.execute();
        } else {
            String cur = "";

            //marker titik perpindahan angkot
            for (final Edge current : hasil) {

                if (current.getParent() != null) {
                    final PolylineOptions polylineOptions = new PolylineOptions();
                    polylineOptions.width(2f);
                    polylineOptions.color(Color.GREEN);
                    polylineOptions.add(new LatLng(current.getParent().getLat(), current.getParent().getLng()), new LatLng(current.getLat(), current.getLng()));
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            map.addPolyline(polylineOptions);
                        }
                    });
                }
                if (current.getLyn().toLowerCase().equals("jalan kaki") || current.getLyn().equals(cur)) {
                    continue;
                }

                cur = current.getLyn();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap icon = Utils.scaleImage(getResources(), R.drawable.busstop, 8);
                        Edge e = current;
                        String title = e.getLyn();
                        Node n = getNode(e.getIdAsal());
                        Marker marker = map.addMarker(new MarkerOptions().position(new LatLng(e.getLat(), e.getLng())).title(n.getNode()).snippet(title).icon(BitmapDescriptorFactory.fromBitmap(icon)));

                        map.addCircle(new CircleOptions()
                                .center(new LatLng(e.getLat(), e.getLng()))
                                .radius(LocationService.RADIUS)
                                .strokeWidth(0.5f)
                                .fillColor(Color.parseColor("#a0ff0000")));
                        marker.showInfoWindow();
                    }
                });
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Bitmap icon = Utils.scaleImage(getResources(), R.drawable.busstop, 8);
                    Edge e = hasil.get(hasil.size() - 1);
                    String title = e.getLyn();
                    Node n = getNode(e.getIdAsal());
                    Marker marker = map.addMarker(new MarkerOptions().position(new LatLng(e.getLat(), e.getLng())).title(n.getNode()).snippet(title).icon(BitmapDescriptorFactory.fromBitmap(icon)));
                    map.addCircle(new CircleOptions()
                            .center(new LatLng(e.getLat(), e.getLng()))
                            .radius(LocationService.RADIUS)
                            .strokeWidth(0.5f)
                            .fillColor(Color.parseColor("#60ff0000")));

                    marker.showInfoWindow();
                }
            });

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        {
            getMenuInflater().inflate(R.menu.map_menu, menu);
            Utils.iconicsMenu(this, menu, R.id.ac_location, MaterialDesignIconic.Icon.gmi_gps_dot.getName());
            Utils.iconicsMenu(this, menu, R.id.ac_navigate, MaterialDesignIconic.Icon.gmi_navigation.getName());
            return true;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.ac_location:
                LatLng current = Utils.getLastLocation(this);
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(current, 18f));
                return true;
            case R.id.ac_navigate:
                if (bottomsheet.getState() != BottomSheetLayout.State.HIDDEN)
                    bottomsheet.dismissSheet();
                else {
                    if (fragmentRute.isAdded()) {
                        bottomsheet.expandSheet();
                    } else {
                        fragmentRute.show(getSupportFragmentManager(), R.id.bottomsheet);
                    }
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void drawInitialLocation() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                map.clear();

                map.addMarker(new MarkerOptions().position(awal).title("awal").icon(BitmapDescriptorFactory.fromBitmap(Utils.scaleImage(getResources(), R.drawable.awal, 32))));
                map.addMarker(new MarkerOptions().position(tujuan).title("tujuan").icon(BitmapDescriptorFactory.fromBitmap(Utils.scaleImage(getResources(), R.drawable.akhir, 32))));

                PolylineOptions polylineOptions = new PolylineOptions();
                polylineOptions.width(2f);
                polylineOptions.color(Color.GREEN);
                polylineOptions.add(awal, new LatLng(startPoint.getLat(), startPoint.getLng()));
                map.addPolyline(polylineOptions);

                PolylineOptions polylineOptions2 = new PolylineOptions();
                polylineOptions2.width(2f);
                polylineOptions2.color(Color.GREEN);
                polylineOptions2.add(tujuan, new LatLng(lastPoint.getLat(), lastPoint.getLng()));
                map.addPolyline(polylineOptions2);

                LatLng lastLocation = (Utils.getLastLocation(MapsActivity.this));

                if (lastLocation == null) {
                    userMarker = map.addMarker(new MarkerOptions().position(new LatLng(0, 0)).icon(BitmapDescriptorFactory.fromBitmap(userIcon)));
                } else {
                    userMarker = map.addMarker(new MarkerOptions().position(lastLocation).icon(BitmapDescriptorFactory.fromBitmap(userIcon)));
                }


            }
        });
    }

    private void reroute(Edge current) {
        drawInitialLocation();

        //tracing jalulr
        while (current != null) {
            hasil.add(0, current);
            if (current.getParent() != null) {
                final PolylineOptions polylineOptions = new PolylineOptions();
                polylineOptions.width(2f);
                polylineOptions.color(Color.GREEN);
                polylineOptions.add(new LatLng(current.getParent().getLat(), current.getParent().getLng()), new LatLng(current.getLat(), current.getLng()));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        map.addPolyline(polylineOptions);
                    }
                });
            }
            current = current.getParent();
        }

        if (hasil == null || hasil.size() == 0)
            return;

        int biaya = 0;
        Edge cur = hasil.get(0);
        LatLng curPos = new LatLng(hasil.get(0).getLat(), hasil.get(0).getLng());
        Node x = getNode(hasil.get(0).getIdAsal());
        textNavigasi.add("Naik " + cur + " di " + x.getNode() + ".");

        //DRAW LOKASI PINDAH + CIRCLE PERTAMA
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Bitmap icon = Utils.scaleImage(getResources(), R.drawable.busstop, 8);
                Edge e = hasil.get(0);
                String title = e.getLyn();
                Node n = getNode(e.getIdAsal());
                Marker marker = map.addMarker(new MarkerOptions().position(new LatLng(e.getLat(), e.getLng())).title(n.getNode()).snippet(title).icon(BitmapDescriptorFactory.fromBitmap(icon)));
                map.addCircle(new CircleOptions()
                        .center(new LatLng(e.getLat(), e.getLng()))
                        .radius(LocationService.RADIUS)
                        .strokeWidth(0.5f)
                        .fillColor(Color.parseColor("#60ff0000")));

                marker.showInfoWindow();
            }
        });

        //marker titik perpindahan angkot
        int i = -1;
        for (final Edge e : hasil) {
            i++;
            if (e.getLyn().toLowerCase().equals("jalan kaki") || e.getLyn().equals(cur.getLyn())) {
                if (i != 0) {
                    textNavigasi.add(textNavigasi.get(i - 1));
                }
                continue;
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Bitmap icon = Utils.scaleImage(getResources(), R.drawable.busstop, 8);
                    String title = e.getLyn();
                    Node n = getNode(e.getIdAsal());
                    Marker marker = map.addMarker(new MarkerOptions().position(new LatLng(e.getLat(), e.getLng())).title(n.getNode()).snippet(title).icon(BitmapDescriptorFactory.fromBitmap(icon)));

                    map.addCircle(new CircleOptions()
                            .center(new LatLng(e.getLat(), e.getLng()))
                            .radius(LocationService.RADIUS)
                            .strokeWidth(0.5f)
                            .fillColor(Color.parseColor("#60ff0000")));

                    marker.showInfoWindow();
                }
            });

            Node n = getNode(e.getIdAsal());

            textNavigasi.add("Turun dari " + cur + ". Naik " + e.getLyn() + " di " + n.getNode() + ".");

            LatLng titikPerpindahan = new LatLng(e.getLat(), e.getLng());

            biaya += 4200;
            double sisa = Math.round(DistanceCalculator.distance(curPos, titikPerpindahan) - 15);
            sisa = sisa > 0 ? sisa : 0;
            biaya += (Math.ceil(sisa * 200));

            curPos = titikPerpindahan;

            cur = e;
        }
        Edge last = hasil.get(hasil.size() - 1);

        Node n = getNode(last.getIdAsal());
        textNavigasi.add("Pemberhentian Terkahir.#13Turun dari " + last.getLyn() + " di " + n.getNode() + ".");

        if (cur.get_id() != last.get_id() && cur.getLyn().equals(last.getLyn())) {
            biaya += 4200;
            double sisa = Math.round(DistanceCalculator.distance(cur.getLat(), cur.getLng(), last.getLat(), last.getLng(), "K") - 15);
            sisa = sisa > 0 ? sisa : 0;
            biaya += (Math.ceil(sisa * 200));
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Bitmap icon = Utils.scaleImage(getResources(), R.drawable.busstop, 8);
                Edge e = hasil.get(hasil.size() - 1);
                String title = e.getLyn();
                Node n = getNode(e.getIdAsal());
                Marker marker = map.addMarker(new MarkerOptions().position(new LatLng(e.getLat(), e.getLng())).title(n.getNode()).snippet(title).icon(BitmapDescriptorFactory.fromBitmap(icon)));
                map.addCircle(new CircleOptions()
                        .center(new LatLng(e.getLat(), e.getLng()))
                        .radius(LocationService.RADIUS)
                        .strokeWidth(0.5f)
                        .fillColor(Color.parseColor("#60ff0000")));

                marker.showInfoWindow();
            }
        });

        final int biayaAkhir = biaya;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                navigasi.setVisibility(View.VISIBLE);

                final Snackbar snackbar = Snackbar.make(navigasi, "Estimasi biaya : " + biayaAkhir, Snackbar.LENGTH_INDEFINITE);

                snackbar.setAction("Dismiss", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.dismiss();
                    }
                }).show();

                fragmentRute.setData(textNavigasi);

                supportInvalidateOptionsMenu();
            }
        });
        startTime = System.currentTimeMillis() - startTime;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MapsActivity.this, "Waktu pencarian : "+(startTime/1000f)+" detik", Toast.LENGTH_LONG).show();
            }
        });
    }

    public List<Edge> getNeighbors(Edge edge) {
        List<Edge> edges = DBAdapter.getInstance(this).getEdges(edge.getIdAkhir());
        List<Edge> result = new ArrayList<>();
        Iterator<Edge> iterator = edges.iterator();
        while (iterator.hasNext()) {
            Edge e = iterator.next();
            if (e == null)
                continue;
            if (closed.contains(e)) {
//                edges.remove(e);
            } else {
                ///jika angkot sama
                if (edge.getLyn().equals(e.getLyn())) {
                    if (astartCustom) {
                        e.setCost(-2);
                    } else {
                        e.setCost(edge.getCost() + 1);
                    }
                //jika angkot beda
                } else {
                    {
                        if (astartCustom) {
                            e.setCost(0);
                        } else {
                            e.setCost(edge.getCost() + 3);
                        }
                    }
                }
                e.setParent(edge);

                //cek apakah parent bisa lanjut ke titik ini. kalo bisa, di ubah rutenya jadi lewat parent
                if (edge.getParent() != null) {

                    if (closed.contains(edge.getParent())) {

                        List<Edge> parentNeighbors = getNeighbors(edge.getParent());
                        Iterator<Edge> parentNeighborIterator = parentNeighbors.iterator();
                        while (parentNeighborIterator.hasNext()) {

                            Edge neighborEdge = parentNeighborIterator.next();

                            if (e.get_id() == neighborEdge.get_id()) {

                                e.setParent(edge.getParent());
                                if (e.getLyn().equals(edge.getLyn())) {
                                    if (astartCustom) {
                                        e.setCost(-2);
                                    } else {
                                        e.setCost(e.getParent().getCost() + 1);
                                    }
                                } else {
                                    {
                                        if (astartCustom) {
                                            e.setCost(0);
                                        } else {
                                            e.setCost(edge.getCost() + 3);
                                        }
                                    }
                                }

                            }
                        }

                        parentNeighborIterator = parentNeighbors.iterator();
                        while (parentNeighborIterator.hasNext()) {
                            Edge neighborEdge = parentNeighborIterator.next();
                            if (neighborEdge.getLyn().equals(e.getLyn())) {
                                if(neighborEdge.getIdAkhir() == edge.getIdAsal()){
                                    e.setParent(edge.getParent());
                                    if (astartCustom) {
                                        e.setCost(-2);
                                    } else {
                                        e.setCost(e.getParent().getCost() + 1);
                                    }
                                }
                            }
                        }

                    }

                }

                e.setHeuristic(DistanceCalculator.distance(new LatLng(e.getLat(), e.getLng()), tujuan));
                result.add(e);
            }
        }
        return result;
    }

    private long startTime;

    private void route() {
        startTime = System.currentTimeMillis();
        List<Edge> starts = DBAdapter.getInstance(this).getEdges(startPoint.get_id());
        for (Edge e : starts) {
            if(astartCustom)
                e.setCost(-20);
            else
                e.setCost(0);

            e.setHeuristic(DistanceCalculator.distance(new LatLng(e.getLat(), e.getLng()), tujuan));
        }
        openlist.addAll(starts);

        while (!openlist.isEmpty() && !killed) {
            Edge e = openlist.poll();
            e.setNode(getNode(e.get_id()));
            openlist.remove(e);
            //draw line
            if (e.getParent() != null) {
                final PolylineOptions polylineOptions = new PolylineOptions();
                polylineOptions.width(2f);
                polylineOptions.color(Color.RED);
                polylineOptions.add(new LatLng(e.getParent().getLat(), e.getParent().getLng()), new LatLng(e.getLat(), e.getLng()));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        map.addPolyline(polylineOptions);
                    }
                });
            }

            //cek titik akhir atau bukan
            if (e.equals(lastPoint)) {
                Edge current = e;
                openlist.add(current);
                //trace jalur
                reroute(current);
                Log.d("ASTAR", "DONE");
                //berhenti
                return;
            }
            //ambil neighbour(titik selanjutnya)
            List<Edge> neighbors = getNeighbors(e);
            openlist.addAll(neighbors);
            closed.add(e);
        }
        Log.d("ASTAR", "NOT FOUND");
    }

    private Node getNode(long id) {
        for (Node n : nodeList) {
            if (n.get_id() == id)
                return n;
        }
        return null;
    }

    @OnClick(R.id.navigasi)
    public void onClick() {
        Utils.setLastPoint(this, tujuan);
        Utils.setStartPoint(this, awal);
        Utils.setNavigatingStatus(this, true);
        Utils.setCurrentNavigationIndex(this, 0);
        Utils.setNavigationText(this, textNavigasi);
        Utils.setRoutes(this, hasil);
        LocationService.alreadyInsideRadius = false;
        navigasi.setVisibility(View.GONE);

        LatLng location = Utils.getLastLocation(this);

        double distance = DistanceCalculator.distance(hasil.get(0).getLat(), hasil.get(0).getLng(), location.latitude, location.longitude, "K") * 1000;
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setAutoCancel(false)
                        .setOngoing(true)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setContentText(distance + " meter lagi\n");

        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        inboxStyle.addLine(Utils.getNavigationText(this).get(0));
        inboxStyle.addLine(distance + " meter lagi");
        mBuilder.setStyle(inboxStyle);
        Intent i = new Intent(this, MapsActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, LocationService.NOTIF_ID, i, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(pendingIntent);
        notificationManager.notify(LocationService.NOTIF_ID, mBuilder.build());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1909) {
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("GPS belum Anda nyalakan. Silahkan nyalakan terlebih dahulu")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivityForResult(intent, 1909);
                            }
                        }).show();
            }
        }
    }

    @Override
    public void onBackPressed() {

        if (bottomsheet.getState() != BottomSheetLayout.State.HIDDEN) {
            bottomsheet.dismissSheet();
            return;
        }

        String message = "Apakah Anda yakin akan menutup navigasi?";

        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Exit")
                .setMessage(message)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        killed = true;

                        Utils.setNavigatingStatus(MapsActivity.this, false);

                        Intent intent = new Intent(MapsActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                        startActivity(intent);
                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.cancel(LocationService.NOTIF_ID);
                        MapsActivity.super.onBackPressed();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    @OnClick({R.id.astar, R.id.astarcustom})
    public void onClick(View view) {
        astar.setVisibility(View.GONE);
        astarcustom.setVisibility(View.GONE);
        switch (view.getId()) {
            case R.id.astar:
                astartCustom = false;
                break;
            case R.id.astarcustom:
                astartCustom = true;
                break;
        }
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                route();
                return null;
            }
        }.execute();
    }

    class RouteListener implements RoutingListener {

        int color;
        int index = 1;

        @Override
        public void onRoutingFailure(RouteException e) {
            Log.d("ROUTING", "FAILED");
            e.printStackTrace();
        }

        @Override
        public void onRoutingStart() {
            Log.d("ROUTING", "START");
        }

        @Override
        public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
//            CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(-7.279443, 112.789629));
//            CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);

//            map.moveCamera(center);

//
//            if(polylines.size()>0) {
//                for (Polyline poly : polylines) {
//                    poly.remove();
//                }
//            }

            polylines = new ArrayList<>();
            //add route(s) to the map.
            for (int i = 0; i < route.size(); i++) {

                //In case of more than 5 alternative routes
                PolylineOptions polyOptions = new PolylineOptions();
                polyOptions.color(color);
                polyOptions.width(5 * index);

                polyOptions.addAll(route.get(i).getPoints());
                Polyline polyline = map.addPolyline(polyOptions);
                polylines.add(polyline);

            }

        }

        @Override
        public void onRoutingCancelled() {

        }
    }
}