package id.ac.pens.student.it.dipta.onlyn;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class StasiunActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stasiun);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ListView list;
        final String[] itemname ={
                "Stasiun Surabaya Kota / Semut",
                "Stasiun Gubeng (SGU)",
                "Stasiun Pasar Turi",
                "Stasiun Wonokromo",
                "Stasiun Tandes",
                "Stasiun Kandangan",
                "Stasiun Benowo"
        };

        Integer[] imgid={
                R.drawable.stasiun,
                R.drawable.stasiun,
                R.drawable.stasiun,
                R.drawable.stasiun,
                R.drawable.stasiun,
                R.drawable.stasiun,
                R.drawable.stasiun,
         };

        final String[] Description ={
                "Jl. Stasiun 9, Bongkaran, Pabean Cantikan, Surabaya, Jawa Timur - 031-3521465",
                "Jl. Gubeng Masjid 1, Pacarkeling, Tambaksari, Surabaya, Jawa Timur - 031-5033115",
                "Jl. Semarang 1, Tembok Dukuh, Bubutan, Surabaya, Jawa Timur - 031-5345014",
                "Jl. Balongsari Tama, Balongsari, Tandes, Surabaya, Jawa Timur",
                "Jl. Stasiun Wonokromo 1, Jagir, Wonokromo, Surabaya, Jawa Timur - 031-8410649",
                "Jl. Raya Tanjung Sari No. 22 Surabaya, Tandes Lor, Tandes, Surabaya, Jawa Timur",
                "Kelurahan Banjarsugihan, Kecamatan Tandes, Surabaya, Jawa Timur",
        };

        CustomListAdapter adapter=new CustomListAdapter(this, itemname, Description, imgid);
        list=(ListView)findViewById(R.id.list);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String Slecteditem= itemname[+position];
                Toast.makeText(getApplicationContext(), Slecteditem, Toast.LENGTH_SHORT).show();

            }
        });
    }

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }


