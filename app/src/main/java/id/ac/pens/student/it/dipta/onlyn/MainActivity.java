package id.ac.pens.student.it.dipta.onlyn;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.context.IconicsLayoutInflater;

import java.util.ArrayList;
import java.util.List;

import id.ac.pens.student.it.dipta.onlyn.utils.LocationService;

public class MainActivity extends AppCompatActivity implements FloatingSearchView.OnQueryChangeListener {

    ViewPager viewpager;
    TabLayout tabs;

    String titles[] = new String[]{
            "gmi-pin",
            "gmi-bus",
            "gmi-city",
            "gmi-info"
    };

    FloatingSearchView mSearchView;

    List<Fragment> pages = new ArrayList<>();

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LayoutInflaterCompat.setFactory(getLayoutInflater(), new IconicsLayoutInflater(getDelegate()));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startService(new Intent(this, LocationService.class));

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pages.add(new NavigationFragment());
        pages.add(new InformationFragment());
        pages.add(new PublicFragment());
        pages.add(new AboutFragment());

        viewpager = (ViewPager) findViewById(R.id.viewpager);
        tabs = (TabLayout) findViewById(R.id.tabs);
        viewpager.setAdapter(new Adapter());
        tabs.setupWithViewPager(viewpager);
        for (int i = 0; i < tabs.getTabCount(); i++) {
            tabs.getTabAt(i).setIcon(new IconicsDrawable(this, titles[i]).color(Color.WHITE).sizeDp(20));
        }

        viewpager.setOffscreenPageLimit(4);

        mSearchView = (FloatingSearchView) findViewById(R.id.floating_search_view);

        mSearchView.setVisibility(View.GONE);
        toolbar.setVisibility(View.VISIBLE);

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
//                        return new NavigationFragment();

                        mSearchView.setVisibility(View.GONE);
                        toolbar.setVisibility(View.VISIBLE);
                        break;
                    case 1:
//                        return new InformationFragment();
                        mSearchView.setVisibility(View.VISIBLE);
                        toolbar.setVisibility(View.GONE);

                        mSearchView.setEnabled(true);
                        mSearchView.setOnQueryChangeListener(null);
                        try {
                            InformationFragment informationFragment = (InformationFragment) pages.get(position);
                            if (informationFragment.getCurrent() instanceof InformationLynFragment) {
                                InformationLynFragment informationLynFragment = (InformationLynFragment) informationFragment.getCurrent();
                                mSearchView.setSearchText(informationLynFragment.getKeyword());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        mSearchView.setOnQueryChangeListener(MainActivity.this);
                        break;
                    case 2:
//                        return new PublicFragment();

                        mSearchView.setVisibility(View.GONE);
                        toolbar.setVisibility(View.VISIBLE);
                        break;
                    case 3:
//                        return new AboutFragment();

                        mSearchView.setVisibility(View.GONE);
                        toolbar.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void onSearchTextChanged(String oldQuery, String newQuery) {
        if (!newQuery.equals(oldQuery)) {

            switch (viewpager.getCurrentItem()) {
                case 0:
//                        return new NavigationFragment();
                    break;
                case 1:
//                        return new InformationFragment();
                    try {
                        InformationFragment informationFragment = ((InformationFragment) ((Adapter) viewpager.getAdapter()).getItem(1));
                        if (informationFragment.getCurrent() instanceof InformationLynFragment) {
                            InformationLynFragment informationLynFragment = (InformationLynFragment) informationFragment.getCurrent();
                            informationLynFragment.setKeyword(newQuery);
                        }
//                        else {
//                            mSearchView.setSearchText(getResources().getString(R.string.app_name));
//                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 2:
//                        return new PublicFragment();

                    break;
                case 3:
//                        return new AboutFragment();
                    break;
            }
        }

    }

    class Adapter extends FragmentPagerAdapter {

        public Adapter() {
            super(getSupportFragmentManager());
        }

        @Override
        public int getCount() {
            return titles.length;
        }


        @Override
        public Fragment getItem(int position) {
            return pages.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;//position> titles.length-1?"":titles[position];
        }
    }

}
