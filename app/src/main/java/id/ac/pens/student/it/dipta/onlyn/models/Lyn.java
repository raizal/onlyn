package id.ac.pens.student.it.dipta.onlyn.models;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import id.ac.pens.student.it.dipta.onlyn.utils.Utils;

/**
 * Created by Dipta on 4/28/2016.
 */
public class Lyn {

    private long _id;
    private String kode;
    private String trayek;
    private String detail;
    private String warna;
    private int berangkat,kembali;
    private List<Edge> edges = new ArrayList<>();

    public int getBerangkat() {
        return berangkat;
    }

    public void setBerangkat(int berangkat) {
        this.berangkat = berangkat;
    }

    public int getKembali() {
        return kembali;
    }

    public void setKembali(int kembali) {
        this.kembali = kembali;
    }

    public static Lyn fromCursor(Cursor cursor){
        return  new Lyn(
                cursor.getLong(cursor.getColumnIndex("id_lyn")),
                cursor.getString(cursor.getColumnIndex("lyn")),
                Utils.unescape(cursor.getString(cursor.getColumnIndex("trayek"))),
                Utils.unescape(cursor.getString(cursor.getColumnIndex("detail"))),
                cursor.getString(cursor.getColumnIndex("warna")),
                cursor.getInt(cursor.getColumnIndex("berangkat")),
                cursor.getInt(cursor.getColumnIndex("kembali"))
        );
    }

    public Lyn(long _id, String kode, String trayek, String detail, String warna, int berangkat, int kembali) {
        this._id = _id;
        this.kode = kode;
        this.trayek = trayek;
        this.detail = detail;
        this.warna = warna;
        this.berangkat = berangkat;
        this.kembali = kembali;
    }

    public Lyn(long _id, String kode, String trayek, String detail, String warna) {
        this._id = _id;
        this.kode = kode;
        this.trayek = trayek;
        this.detail = detail;
        this.warna = warna;
    }



    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getTrayek() {
        return trayek;
    }

    public void setTrayek(String trayek) {
        this.trayek = trayek;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getWarna() {
        return warna;
    }

    public void setWarna(String warna) {
        this.warna = warna;
    }
}
